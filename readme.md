Modification de fichiers ?
- Le job jobuild ne se lance que lors de modifications sur les fichiers composer.lock et/ou composer.json

Création d'un tag ?
- Le job jobdeploy ne se lance que lors de la création d'un nouveau tag de la forme X.X.X (ex. tag 1.5.2). Pour en créer un nouveau : git tag x.x.x, git push origin x.x.x

Artifact pour définir les paths ou sauvegarder des logs ?
- Un artifact est utilisé au sein du deploy pour sauvegarder un fichier nommé file.log, ce fichier se trouve dans Build/Artifacts et en téléchargeant le dossier artifacts.zip dans jobdeploy

Différentes clauses utilisables :
- When : always pour toujours lancer un job
- Needs : job pour lancer un job du même stage successivement au lieu d'une exécution parallèle
- Allow_failure: true : s'il y a échec, la suite de la pipeline est quand même exécutée
- Dependencies : pour permettre l'exécution séquentielle de jobs (attention, clause obsolète)
































autre manière de faire : 

image: docker:latest

services:
  - docker:dind

stages:
  - rebuild

before_script:
  - apk add --no-cache docker-compose # Installing Docker Compose

rebuild_project:
  stage: rebuild
  script:
    - cp .env.example .env
    - cp drush/example.drush.yml drush/drush.yml
    - docker-compose up -d
    # If you have a specific command for database import, add it below
    #- your-db-import-command
    - docker-compose exec -T php composer install
    # If you have a specific command to extract files from files.tar.gz, add it below
    #- your-extract-command
  only:
    changes:
      - composer.json
      - composer.lock